��          �   %   �      P     Q     m     �     �     �     �  	   �     �     �     �     �     �     �     �                 	     8   $     ]     n     t     �  "   �     �  �  �  &   �  5   �     
     )     2     R     c          �     �  $   �     �     �            
   -     8     D  [   X     �     �     �     �  ,   �  
   ,           
                                  	                                                                                - You must provide a value! - You must select a brand! Add new model Body Brand Cars Cars list Choose a model Color Colors list Delete vehicles cards Member's cars Model Models list My cars Name Name: New model No vehicle was selected, please check at least one name. Remove %1$s %2$s State Transmission Vehicles list You do not have enough privileges. model Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-19 19:38+0000
Last-Translator: ButterflyOfFire <boffire@users.noreply.hosted.weblate.org>
Language-Team: Arabic <https://hosted.weblate.org/projects/galette/auto-plugin/ar/>
Language: ar
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 ? 4 : 5;
X-Generator: Weblate 5.1
 - يجب عليك تقديم قيمة! - يجب عليك تحديد علامة تجارية! إضافة نموذج جديد هيكل العلامة التجارية السيارات لائحة السيارات احتر نموذجًا لون قائمة الألوان حذف بطاقات المركبات سيارات العضو النموذج قائمة النماذج سياراتي الإسم الإسم: نموذج جديد لم يتم إختيار اي مركبة، رجاء اختر اسم واحد على اقل. إزالة %1$s %2$s حالة الانتقال لائحة المركبات ليس لديك امتيازات كافية. نموذج 