��    6      �  I   |      �      �  /   �  <   �  :   /  6   j     �     �  -   �          
       )        ?  	   K     U     i     o     �     �     �     �     �     �     �     �     �     �     �  8         9     @     Q     e     u     {     �     �     �  "   �     �     �     �     �     �       
   (     3     ;     A     F     S     Y     _  �  l  !   

  0   ,
  Z   ]
  Z   �
  J     #   ^  &   �  @   �     �     �       X        p     �     �     �     �     �  	   �  &   �               "     )     8     D     I     a  L   }     �     �     �     �                 &   '     N     \     |     �  
   �     �     �     �  %   �                         0     ?     E        0       '          (   "          !           *       #                +                          6   $          	                 /          5             &       2   1      )   3      ,   %   -                   .         4                            
    %count finition %count finitions %count vehicles have been successfully deleted. - An error has occured while saving vehicle in the database. - An error occurred while saving record. Please try again. - No id provided for modifying this record! (internal) - You must provide a value! - You must select a brand! An error occured trying to remove vehicles :/ Bio Body Brand Car ID cannot be null calling edit route! Car updated Cars list Change vehicle '%s' Color Delete vehicles cards Diesel Electricity Fail to add new car. Gas History of car #%d Hybrid Member's cars My cars Name New car added New vehicle No vehicle was selected, please check at least one name. Petrol Remove %1$s %2$s Remove vehicle %1$s Remove vehicles State Transmission Vehicle Vehicle has been saved! Vehicles list You do not have enough privileges. body color engine size finition first circulation date first registration date horsepower mileage model name registration seats state transmission Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-08 08:09+0000
Last-Translator: Johan Cwiklinski <trasher@x-tnd.be>
Language-Team: Italian <https://hosted.weblate.org/projects/galette/auto-plugin/it/>
Language: it_IT
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.0-dev
 %count finizione %count finizioni %count veicoli sono stati rimossi correttamente. - C'è stato un errore durante il salvataggio delle informazioni del veicolo nel database. - C'è stato un errore durante il salvataggio delle informazioni del veicolo nel database. - non è stato fornito alcun id per modificare questo documento! (interno) - è necessario indicare un valore! - è necessario selezionare una marca! C'è stato un errore mentre si tentava di rimuovere i veicoli :/ bio-carburante Stato della carrozzeria Marca Deve essere indicata la targa dell'auto quando si richiede una modifica dell'itinerario! Auto aggiornata Lista delle auto Modificare il veicolo '%s' Colore Cancellare le schede veicolo Diesel Elettrico Impossibile aggiungere una nuova auto. Gas Storico dell'auto #%d Ibrido Auto del socio Le mie auto Nome Aggiunta una nuova auto Aggiungere un nuovo veicolo Non è stato selezionato alcun veicolo, per favore inserirne almeno un tipo. Benzina Rimuovi %1$s %2$s Eliminare il veicolo %1$s Eliminare i veicoli Stato Cambio Veicolo I dati del veicolo sono stati salvati! Lista veicoli Non hai qualifiche sufficienti. carrozzeria colore cilindrata finitura Data di prima circolazione Data di prima immatricolazione potenza dell'auto (numero di cavalli) chilometraggio modello Nome Immatricolazione posti a sedere stato cambio 