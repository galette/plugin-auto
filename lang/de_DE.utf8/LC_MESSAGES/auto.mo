��    H      \  a   �             !     B  (   ]     �  /   �  <   �  :     6   M     �     �     �     �  -   �          	       )        >  	   J     T     f     z     �     �     �     �     �     �     �     �     �     �     �     	     	     #	     /	     7	     <	     B	     F	  	   T	     ^	     x	  8   �	     �	     �	  $   �	     �	     
     "
     2
     8
     D
     Q
     Y
     q
  "   
     �
     �
     �
     �
     �
  
   �
     �
     �
     �
     �
                 �        �     �     �        -   9  L   g  U   �  =   
  !   H  $   j     �     �  9   �     �  
   �       &        5     G     U     l     �  	   �  	   �     �     �     �  *   �     �     �     �               1     8     R     ^     n     s     y     }     �      �     �  R   �     %     ,  ,   A     n     �     �     �     �     �     �     �     �  )   �  
   )     4     :     H     Q     i     w     �     �  	   �     �     �     �                9   F      (   +   %           =          1          ;   A            ?   6      4   :   #   2              	       3               >      
          $   H   C       &               '   @                 D   E                            "            )   ,   .   8          G                     7   5   0   B       <      /      -   *       !    %count finition %count finitions %count state %count states %count transmission %count transmissions %count vehicle %count vehicles %count vehicles have been successfully deleted. - An error has occured while saving vehicle in the database. - An error occurred while saving record. Please try again. - No id provided for modifying this record! (internal) - You must provide a value! - You must select a brand! Add new color Add new state An error occured trying to remove vehicles :/ Bio Body Brand Car ID cannot be null calling edit route! Car updated Cars list Change model '%s' Change vehicle '%s' Color Colors list Comment Delete vehicles cards Diesel Electricity Fail to add new car. Finition Gas History of car #%d Hybrid Member's cars Model Model has been saved! Models list My cars Name Name: New New car added New model New model has been added! New vehicle No vehicle was selected, please check at least one name. Petrol Remove %1$s %2$s Remove %1$s model Remove %1$s models Remove model "%1$s" Remove vehicle %1$s Remove vehicles State States list Transmission Vehicle Vehicle has been saved! Vehicles list You do not have enough privileges. body color engine size finition first registration date horsepower mileage model name registration seats state transmission Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-09-20 00:26+0000
Last-Translator: Johan Cwiklinski <trasher@x-tnd.be>
Language-Team: German <https://hosted.weblate.org/projects/galette/auto-plugin/de/>
Language: de_DE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 5.1-dev
 %count finition %count finitions %count Status %count Status %count Getriebe %count Getriebe %count Fahrzeug %count Fahrzeuge %count Fahrzeuge wurden erfolgreich entfernt. - Beim Speichern des Fahrzeuges in der Datenbank ist ein Fehler aufgetreten. - Beim Speichern der Aufzeichnung ist ein Fehler aufgetreten. Bitte erneut probieren. - Keine ID zum Ändern dieses Datensatzes angegeben! (intern) - Sie müssen einen Wert angeben! - Sie müssen eine Marke auswählen! Neue Farbe hinzufügen Neuen Status hinzufügen Beim Löschen der Fahrzeuge ist ein Fehler aufgetreten :/ Bio Karosserie Marke Die ID des Autos darf nicht null sein! Auto aktualisiert Fahrzeugliste Modell '%s' bearbeiten Fahrzeug ändern '%s' Farbe Farbliste Kommentar Fahrzeugkarten löschen Diesel Strom Neues Auto kann nicht hinzugefügt werden. finition Benzin Geschichte des Autos #%d Hybrid Autos der Mitglieder Modell Modell wurde gespeichert! Modellliste Meine Fahrzeuge Name Name: Neu Neues Auto hinzugefügt Neues Modell Neues Modell wurde hinzugefügt! Neues Fahrzeug Es wurde kein Fahrzeug ausgewählt, bitte überprüfen Sie mindestens einen Namen. Benzin %1$s·%2$s entfernen %1$s Modell entfernen %1$s Modelle entfernen Modell %1$s entfernen Fahrzeug %1$s entfernen Entferne Farhzeuge Zustand Statusliste Getriebe Fahrzeug Fahrzeug wurde gespeichert! Fahrzeugliste Sie haben nicht genügend Berechtigungen. Karosserie Farbe Motorisierung finition Datum der Erstzulassung Pferdestärke Kilometerstand Modell Name Zulassung Sitzplätze Status Getriebe 